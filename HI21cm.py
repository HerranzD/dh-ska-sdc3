#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 22:17:13 2023

@author: dherranz
"""

# %% ---- IMPORTS -------------

import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u 
import os

# %% ---- 21cm LINE -------------

lambda21cm = 21.106114054160*u.cm
freq21cm   = lambda21cm.to(u.MHz,equivalencies=u.spectral())

# %% ---- REDSHIFT -------------

def redshift_at(values):
    """
    Returns the redshift at witch the 21cm HI line is seen at frequency or
    wavelength 'values'

    Parameters
    ----------
    values : astropy.Quantity or array
        Frequency or wavelenght of an observed line or set of lines.

    Returns
    -------
    int or array
        The redshift of the observed line.

    """
    l = values.to(u.cm,equivalencies=u.spectral())
    return (l/lambda21cm).si.value - 1

def lambda_at_z(z):
    """
    Returns the wavelength of the HI line at redshift z

    Parameters
    ----------
    z : float
        Redshift.

    Returns
    -------
    ~astropy.Quantity
        Wavelength at z.

    """
    return (1+z)*lambda21cm

def freq_at_z(z):
    """
    Returns the frequency of the HI line at redshift z

    Parameters
    ----------
    z : float
        Redshift.

    Returns
    -------
    ~astropy.Quantity
        Frequency at z.

    """
    return lambda_at_z(z).to(u.MHz,equivalencies=u.spectral())