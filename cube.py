#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 23:01:46 2023

@author: dherranz
"""

import numpy             as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import astropy.units     as u
import healpy            as hp
import warnings 
import SDC3_definitions  as SDC3defs
       
from astropy.wcs         import WCS
from astropy.io          import fits
from astropy.coordinates import SkyCoord
from HI21cm              import redshift_at
from matched_filter      import matched_filter as mf
from matched_filter      import new_iterative_matched_filter as itmf
from gauss2dfit          import fit_single_peak
from astropy.wcs         import FITSFixedWarning
from astropy.table       import QTable
from astropy.nddata      import Cutout2D
from astropy.wcs.utils   import pixel_to_skycoord
from astropy.wcs.utils   import skycoord_to_pixel

# %%   BASIC CONSTANTS AND DEFINITIONS

fwhm2sigma   = 1.0/(2.0*np.sqrt(2.0*np.log(2.0)))
sigma2fwhm   = 1.0/fwhm2sigma


class Slice:
    """
    A class to represent a 2D slice of a 3D FITS cube.

    Attributes
    ----------
    islice : int
        The index of the slice.
    data : numpy.ndarray
        A 2D numpy array containing the slice data.
    psf : numpy.ndarray
        A 2D numpy array containing the point spread function for the slice.
    header : astropy.io.fits.header.Header
        The FITS header for the cube.
    wcs : astropy.wcs.WCS
        The world coordinate system for the slice.
    weighting : str
        The weighting used for the slice (either 'uniform' or 'natural').

    Methods
    -------
    load(islice, weighting)
        Loads a 2D slice of a 3D FITS cube from file.
    plot(title_z=True)
        Plots the slice.
    plot_psf(title_z=True)
        Plots the point spread function for the slice.
    pixel_coordinate(i, j)
        Returns the sky coordinate of a given pixel of the slice.
    coordinate_pixel(coord)
        Returns the pixel index of the pixel nearest to a given coordinate.
    pixel2healpix(i, j, nside, coordsys='G', nested=False)
        Converts pixel coordinates to HEALPix coordinates.

    Properties
    ----------
    size : int
        The size of the 2D slice.
    i : int
        The index of the slice.
    freq : astropy.units.quantity.Quantity
        The frequency of the slice.
    wavelength : astropy.units.quantity.Quantity
        The wavelength of the slice.
    redshift21cm : float
        The redshift of the 21cm line for the slice.
    wcs2 : astropy.wcs.WCS
        The 2D world coordinate system for the slice.


    """
    def __init__(self,islice,data,psf,header,wcs,weighting):
        """
        Instantiates a `Slice` object.

        Parameters
        ----------
        islice : int
            The index of the slice.
        data : numpy.ndarray
            A 2D numpy array containing the slice data.
        psf : numpy.ndarray
            A 2D numpy array containing the point spread function for the slice.
        header : astropy.io.fits.header.Header
            The FITS header for the cube.
        wcs : astropy.wcs.WCS
            The world coordinate system for the slice.
        weighting : str
            The weighting used for the slice (either 'uniform' or 'natural').

        Returns
        -------
        Slice
            A `Slice` object.

        """
        self.islice    = islice
        self.data      = data
        self.psf       = psf
        self.header    = header
        self.wcs       = wcs
        self.weighting = weighting
        
        
# %% ------- BASIC OPERATORS --------------------------------------------------

    def __copy__(self):
        """
        Create a shallow copy of the object.

        Returns:
            New Slice: A shallow copy of the object with the same attribute values.
        """
        new_obj = Slice(self.islice, 
                        self.data, 
                        self.psf, 
                        self.header, 
                        self.wcs, 
                        self.weighting)
        return new_obj    


    def __add__(self, other):
        """
        Add the data arrays of two Slice objects element-wise.

        Args:
            other (Slice): Another Slice object to add to this one.

        Returns:
            Slice: A new Slice object with the same attributes as this one, except with
            the data array equal to the element-wise sum of this object's data array and the
            other object's data array.
        """
        new_data = np.add(self.data, other.data)
        
        return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)    
    
    def __iadd__(self, other):
        """
        Add the data arrays of two Slice objects element-wise, to be used with +=.

        Args:
            other (Slice): Another Slice object to add to this one.

        Returns:
            Slice: A new Slice object with the same attributes as this one, except with
            the data array equal to the element-wise sum of this object's data array and the
            other object's data array.
        """
        new_data = np.add(self.data, other.data)
        
        return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)   
    
    def __sub__(self, other):
       """
       Subract the data arrays of two Slice objects element-wise.

       Args:
           other (Slice): Another Slice object to subtract from this one.

       Returns:
           Slice: A new Slice object with the same attributes as this one, except with
           the data array equal to the element-wise subtraction of this object's data 
           array and the other object's data array.
       """
       new_data = np.subtract(self.data, other.data)
       
       return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)   
   
    def __isub__(self, other):
      """
      Subract the data arrays of two Slice objects element-wise, to be used with -=.

      Args:
          other (Slice): Another Slice object to subtract from this one.

      Returns:
          Slice: A new Slice object with the same attributes as this one, except with
          the data array equal to the element-wise subtraction of this object's data 
          array and the other object's data array.
      """
      new_data = np.subtract(self.data, other.data)
      
      return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)   
   
    def __mul__(self, other):
       """
       Multiply the data arrays of two Slice objects element-wise.

       Args:
           other (Slice): Another Slice object to multiply to this one.

       Returns:
           Slice: A new Slice object with the same attributes as this one, except with
           the data array equal to the element-wise multiplication of this object's data 
           array and the other object's data array.
       """
       new_data = np.multiply(self.data, other.data)
       
       return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)  
   
    def __div__(self, other):
       """
       Divide the data arrays of two Slice objects element-wise.

       Args:
           other (Slice): Another Slice object by which this one will be divided.

       Returns:
           Slice: A new Slice object with the same attributes as this one, except with
           the data array equal to the element-wise division of this object's data 
           array and the other object's data array.
       """
       new_data = np.divide(self.data, other.data)
       
       return Slice(self.islice, new_data, self.psf, self.header, self.wcs, self.weighting)  

# %% ------- BASIC PROPERTIES -------------------------------------------------

    @property
    def size(self):
        """
        Returns the size of the data array in Slice.

        Returns
        -------
        int
            Data array size.

        """
        return self.data.size
    
    @property 
    def shape(self):
        """
        Returns the shape of the data array in the Slice

        Returns
        -------
        tuple
            The shape of the data array.

        """
        return self.data.shape
    
    @property 
    def pixsize(self):
        """
        Returns the pixel size of the data in the Slice, in arcseconds

        Returns
        -------
        astropy.units.quantity.Quantity
            The pixel size of the data.

        """
        w = self.wcs2
        p = np.abs(w.pixel_scale_matrix).max()*u.deg
        return p.to(u.arcsec)
        


# %% ------- INPUT/OUTPUT ----------------------------------------------------

    @classmethod
    def load(self,islice,weighting):
        """
        Loads a Slice form the SDC3 data cube.

        Parameters
        ----------
        islice : int
            The index along the first dimension (frequency).
        weighting : str
            Can be 'uniform' or 'natural'
            
        Returns
        -------
        Slice
            Slice at the position (frequency) islice.

        """
        
        warnings.filterwarnings('ignore', category=FITSFixedWarning)
        
        if weighting == 'uniform':
            fname = SDC3defs.unif_cube_name
            psfn  = SDC3defs.unif_psf_name
        else:
            fname = SDC3defs.nat_cube_name
            psfn  = SDC3defs.nat_psf_name

        hdu = fits.open(fname)
        h   = hdu[0].header
        wcs = WCS(h)

        imageslice = hdu[0].data[islice,:,:]
        hdu.close()

        hdu = fits.open(psfn)
        psfslice = hdu[0].data[islice,:,:]
        hdu.close()

        return Slice(islice, imageslice, psfslice,h,wcs,weighting)
    
# %% ------- PSF FITTING ------------------------------------------------------    

    def psf_fit(self,psize=64,verbose=False,toplot=False):
        """
        Fits the PSF in the Slice to a Gaussian profile

        Parameters
        ----------
        psize : int, optional
            The size of the central square of the psf image that will be fitted. 
            The default is 64.
        verbose : bool, optional
            If True, writes some info on screen. The default is False.
        toplot : bool, optional
            If True, plots the fit on a separate window. The default is False.

        Returns
        -------
        fwhm : astropy.units.quantity.Quantity
            The FWHM of the Gaussian that better fits the PSF.

        """
        
        c = self.shape[0]//2
        p = psize//2
        minipatch = self.psf[c-p:c+p-1,c-p:c+p-1]
        f         = fit_single_peak(minipatch)
        fwhm_pix  = sigma2fwhm*f.sigma
        fwhm      = fwhm_pix*self.pixsize
        
        if verbose:
            print(' ')
            print(' Gaussian fit parameters:')
            print('     X center     = ',f.x)
            print('     Y center     = ',f.y)
            print('     Amplitude    = ',f.amplitude)
            print('     Sigma (pix)  = ',f.sigma)
            print('     Residual rms = ',f.residual.std())
            print(' ')
            
        if toplot:
            f.plot()
        
        return fwhm
    
    @property 
    def fwhm(self):
        """
        Returns the FWHM of the best-fit Gaussian approximation to the PSF

        Returns
        -------
        astropy.units.quantity.Quantity
            FWHM of the best-fit Gaussian approximation to the PSF.

        """
        return self.psf_fit()
    
    @property 
    def fwhm_pix(self):
        """
        Returns the FWHM of the best-fit Gaussian approximation to the PSF, 
        in pixel units

        Returns
        -------
        float
            FWHM of the best-fit Gaussian approximation to the PSF, 
            in pixel units.

        """
        return (self.fwhm/self.pixsize).si.value


# %% ------- PLOTTING --------------------------------------------------------

    def plot(self,
             title_z = True,
             stamp   = False,
             ij      = (1024,1024),
             lsize   = 64):
        """
        Plots the data in the Slice

        Parameters
        ----------
        title_z : bool, optional
            If True, the title of the plot will come in redshift units.
            If False, the tile of the plot will como in MHz. The default is True.
        stamp : bool, optional
            If True, the method plots a poststamp of the data. If False, it plots
            the whole image. The default is False.
        ij : tuple, optional
            The (i,j) values of the central pixel of the poststamp. This argument
            is used only if stamp=True. The default is (1024,1024).
        lsize : int, optional
            The size in pixels of the poststamp. The default is 64.

        Returns
        -------
        None.

        """
             
        if stamp:
            coord      = self.pixel_coordinate(ij[0],ij[1])
            cutout     = Cutout2D(self.data, 
                                  position = coord, 
                                  size     = lsize, 
                                  wcs      = self.wcs2,
                                  mode     = 'partial')
            imageslice = cutout.data
            wcs        = cutout.wcs
        else:
            imageslice = self.data
            wcs        = self.wcs2
            
        freq  = self.freq
    
        fig = plt.figure()
        fig.add_subplot(111, projection=wcs)
        plt.imshow(imageslice, origin='lower',
                   cmap=plt.cm.viridis,
                   norm=colors.SymLogNorm(linthresh=0.003,
                                          linscale=1,
                                          vmin=imageslice.min(),
                                          vmax=imageslice.max(),
                                          base=10))
    
        plt.colorbar()
        plt.xlabel('RA')
        plt.ylabel('Dec')
        if title_z:
            plt.title(r'$z$={0}'.format(self.redshift21cm))
        else:
            plt.title('{0} MHz'.format(freq.value))

    def plot_psf(self,title_z=True):
        """
        Plots the PSF of the Slice. 

        Parameters
        ----------
        title_z : bool, optional
            If True, the title of the plot will come in redshift units.
            If False, the tile of the plot will como in MHz. The default is True.

        Returns
        -------
        None.

        """
    
        imageslice = self.psf
        freq  = self.freq
    
        fig = plt.figure()
        fig.add_subplot(111, projection=self.wcs2)
        plt.imshow(imageslice, origin='lower',
                   cmap=plt.cm.viridis,
                   norm=colors.SymLogNorm(linthresh=0.003,
                                          linscale=1,
                                          vmin=imageslice.min(),
                                          vmax=imageslice.max(),
                                          base=10))
    
    
        plt.colorbar()
        plt.xlabel('RA')
        plt.ylabel('Dec')
        if title_z:
            plt.title(r'$z$={0}'.format(self.redshift21cm))
        else:
            plt.title('{0} MHz'.format(freq.value))

# %% ------- INDEXING, FREQUENCY, REDSHIFT -----------------------------------

    @property
    def i(self):
        """
        Returns the index of a Slice inside the data cube

        Returns
        -------
        int
            The index of the Slice.

        """
        return self.islice

    @property
    def freq(self):
        """
        Returns the SKA frecuency of observation of this Slice.

        Returns
        -------
        astropy.units.quantity.Quantity
            The frequency of the Slice.

        """
        i = self.islice
        return (self.wcs.pixel_to_world(0,0,i,0)[1]).to(u.MHz)

    @property
    def wavelenght(self):
        """
        Returns the SKA wavelenght of observation of this Slice.

        Returns
        -------
        astropy.units.quantity.Quantity
            The fwavelength of the Slice.

        """
        return (self.freq).to(u.cm,equivalencies=u.spectral())

    @property
    def redshift21cm(self):
        """
        Returns the redshift of the 21 cm HI line corresponding to the
        observation frequency of the Slice.

        Returns
        -------
        float
            The redshift.

        """
        return redshift_at(self.freq)

# %% ------- SKY COORDINATES -------------------------------------------------

    @property
    def wcs2(self):
        return self.wcs.sub(2)

    def pixel_coordinate(self,i,j):
        """
        Returns the sky coordinate of a given pixel of the `Slice`.

        Parameters
        ----------
        i : int or float
            The pixel index along the x-axis.
        j : int or float
            The pixel index along the y-axis.

        Returns
        -------
        `~astropy.coordinates.SkyCoord`
            The sky coordinate of the i,j pixel.

        """
        return pixel_to_skycoord(i,j,self.wcs2,origin=1)

    def coordinate_pixel(self,coord):
        """
        Returns the pixel index of the pixel nearest to a given coordinate.

        Parameters
        ----------
        coord : `~astropy.coordinates.SkyCoord`
            A coordinate in the sky.

        Returns
        -------
        float
            The pixel index along the x-axis.
        float
            The pixel index along the y-axis.

        """
        
        return skycoord_to_pixel(coord,self.wcs2,origin=1)
        

    def pixel2healpix(self,i,j,nside,coordsys='G',nested=False):
        """
        Returns the HEALPix pixel nearest to the i,j pixel of the Slice

        Parameters
        ----------
        i : int
            Pixel index along the x axis.
        j : int
            Pixel index along the y axis.
        nside : int
            HEALPix nside parameter.
        coordsys : str, optional
            Coordinate system of the HEALPix map. The default is 'G', for Galactic
        nested : bool, optional
            If True, uses the NESTED ordering. Otherwise, it uses the RING scheme. 
            The default is False.

        Returns
        -------
        ipix : int
            The HEALPix pixel corresponding to the (i,j) pixel of the Slice.

        """

        coord = self.pixel_coordinate(i,j)
        if coordsys.upper()[0] == 'G':
            lon = coord.galactic.l.deg
            lat = coord.galactic.b.deg
        else:
            lon = coord.icrs.ra.deg
            lat = coord.icrs.dec.deg
        ipix = hp.ang2pix(nside,lon,lat,nest=nested,lonlat=True)
        return ipix

    def healpix2pixel(self,ipix,nside,coordsys='G',nested=False):
        """
        Returns the (i,j) pixel of the Slice nearest a given HEALPix pixel

        Parameters
        ----------
        ipix : int
            HEALPix pixel.
        nside : int
            HEALPix nside parameter.
        coordsys : str, optional
            Coordinate system of the HEALPix map. The default is 'G', for Galactic
        nested : bool, optional
            If True, uses the NESTED ordering. Otherwise, it uses the RING scheme. 
            The default is False.

        Returns
        -------
        tuple
            The Slice pixel corresponding to the HEALPix pixel. This is a tuple
            of floats. It may be convenient to round it to integers, for some
            applications.

        """

        lon,lat = hp.pix2ang(nside,ipix,nest=nested,lonlat=True)
        if coordsys.upper()[0] == 'G':
            c = SkyCoord(lon,lat,unit=u.deg,frame='galactic')
        else:
            c = SkyCoord(lon,lat,unit=u.deg,frame='icrs')
        return self.coordinate_pixel(c)

# %% ------- MATCHED FILTERING -----------------------------------------------

    @property
    def psf_template(self):
        """
        Returns a normalized version of the Slice PSF

        Returns
        -------
        numpy.array
            A 2D image containing the PSF normalized so that the maximum
            value is equal to one.

        """
        return self.psf/self.psf.max()
    
    def matched_filter(self,nbins=512):
        """
        Return a matched filtered version of the Slice.data. This methods uses
        the Slice.psf as source profile.

        Parameters
        ----------
        nbins : int, optional
            Number of bins for the calculation of the power spectrum of the image. 
            The default is 512.

        Returns
        -------
        Slice
            A new Slice whose data contains a matched filtered version of the
            data in the original Slice.

        """
        
        m = mf(self.data,
               gprof  = False,
               tprof0 = self.psf_template,
               nbins  = nbins)
        
        return Slice(self.islice,m,
                     self.psf,
                     self.header,
                     self.wcs,
                     self.weighting)
    
    
    def ideal_matched_filter(self,nbins=512):
        """
        Return a matched filtered version of the Slice.data. This methods uses
        an ideal Gaussian beam with FWHM=Slice.fwhm_pix as source profile.

        Parameters
        ----------
        nbins : int, optional
            Number of bins for the calculation of the power spectrum of the image. 
            The default is 512.

        Returns
        -------
        Slice
            A new Slice whose data contains a matched filtered version of the
            data in the original Slice.

        """
        
        m = mf(self.data,
               lafwhm = self.fwhm_pix,
               nbins  = nbins)
        
        
        return Slice(self.islice,
                     m,
                     self.psf,
                     self.header,
                     self.wcs,
                     self.weighting)
    
    def ideal_iterative_matched_filter(self,nbins=512):
        """
        Return an iterative matched filtered version of the Slice.data. 
        This methods uses the Slice.psf as source profile. The use of this method
        is not recommended as it takes a lot computing time

        Parameters
        ----------
        nbins : int, optional
            Number of bins for the calculation of the power spectrum of the image. 
            The default is 512.

        Returns
        -------
        Slice
            A new Slice whose data contains a matched filtered version of the
            data in the original Slice.

        """
        
        m = itmf(self.data, 
                 self.fwhm_pix,
                 nbins = nbins)
        
        return Slice(self.islice,
                     m['Filtered'],
                     self.psf,
                     self.header,
                     self.wcs,
                     self.weighting) 

 
# ============================================================================    


def plot_cubeslice(islice, cube_type='uniform'):
    """
    Plots a slice of a cube in either uniform or natural weighting.

    Parameters
    ----------
    islice : int
        The index of the slice to be plotted.
    cube_type : str, optional
        The type of cube to be plotted. 'uniform' for uniform weighting and
        'natural' for natural weighting. Default is 'uniform'.

    Returns
    -------
    None

    Notes
    -----
    This routine opens a FITS file containing a data cube and extracts a 2D
    slice from the cube. The slice is then plotted using a logarithmic color
    scale and labeled with the corresponding frequency.

    This routine depends on the following external modules:
        numpy
        matplotlib.pyplot
        matplotlib.colors
        astropy.units
        astropy.wcs.WCS
        astropy.io.fits
        SDC3_definitions

    """



    if cube_type == 'uniform':
        fname = unif_cube_name
    else:
        fname = nat_cube_name

    hdu = fits.open(fname)
    h   = hdu[0].header
    wcs = WCS(h)
    w2  = wcs.sub(2)

    imageslice = hdu[0].data[islice,:,:]
    freq  = wcs.pixel_to_world(0,0,islice,0)[1]

    fig = plt.figure()
    fig.add_subplot(111, projection=w2)
    plt.imshow(imageslice, origin='lower',
               cmap=plt.cm.viridis,
               norm=colors.SymLogNorm(linthresh=0.003,
                                      linscale=1,
                                      vmin=imageslice.min(),
                                      vmax=imageslice.max(),
                                      base=10))


    plt.colorbar()
    plt.xlabel('RA')
    plt.ylabel('Dec')
    plt.title('{0} MHz'.format(freq.to(u.MHz).value))

    hdu.close()


def get_fwhms():
    
    fname = unif_cube_name
    hdu   = fits.open(fname)
    L     = hdu[0].data.shape[0]
    hdu.close()
    
    islice = []
    freq   = []
    z      = []
    fwhm   = []
    
    for i in range(L):
        print('Slice {0} of uniform weighting cube'.format(i))
        s = Slice.load(i,'uniform')
        islice.append(i)
        freq.append(s.freq)
        z.append(s.redshift21cm)
        fwhm.append(s.fwhm)
    
    dicto = {'slice':islice,
             'freq':freq,
             'z':z,
             'fwhm':fwhm}
    
    tu = QTable(dicto)
    tu.write('/home/dherranz/SDC3/Data/fwhm_table_uniform.fits',overwrite=True)
    
    fname = nat_cube_name
    hdu   = fits.open(fname)
    L     = hdu[0].data.shape[0]
    hdu.close()
    
    islice = []
    freq   = []
    z      = []
    fwhm   = []
    
    for i in range(L):
        print('Slice {0} of natural weighting cube'.format(i))
        s = Slice.load(i,'natural')
        islice.append(i)
        freq.append(s.freq)
        z.append(s.redshift21cm)
        fwhm.append(s.fwhm)
    
    dicto = {'slice':islice,
             'freq':freq,
             'z':z,
             'fwhm':fwhm}
    
    tn = QTable(dicto)
    tn.write('/home/dherranz/SDC3/Data/fwhm_table_natural.fits',overwrite=True)
    
    return tu,tn
    