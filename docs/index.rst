.. DH_SKA_SDC3 documentation master file, created by
   sphinx-quickstart on Tue Apr  4 11:23:57 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DH_SKA_SDC3's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
