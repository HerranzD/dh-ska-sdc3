#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 22:04:58 2023

@author: dherranz
"""


# %% ---- PATHS TO DATA -------------

data_dir = '/mnt/sdc3a.MS.0/sdc3dataset/'
img_dir  = data_dir+'Image/Image/'

unif_cube_name = img_dir+'ZW2.msw_image.fits'
unif_psf_name  = img_dir+'ZW2.msw_psf.fits'

nat_cube_name  = img_dir+'ZW2.msn_image.fits'
nat_psf_name   = img_dir+'ZW2.msn_psf.fits'

